// https:api.openweathermap.org/data/2.5/forecast?q=Mumbai&units=metric&appid=1aa5437a4cc6cb9c7112238f40ba6a86

//https://openweathermap.org/api


const key = "1aa5437a4cc6cb9c7112238f40ba6a86"; //api key
const getForecast = async(city) => {
    const base = "https://api.openweathermap.org/data/2.5/forecast"
    const query = `?q=${city}&units=metric&appid=${key}`; //by city name

    const response = await fetch(base + query);

    //console.log(response);

    if (!response.ok)
        throw new Error("Status Code:" + response.status); //if 404

    const data = await response.json();
    //console.log(data);
    return data;
}

getForecast('Mumbai')
    .then(data => console.log(data))
    .catch(err => console.warn(err));